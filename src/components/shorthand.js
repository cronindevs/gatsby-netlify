import React from "react"
const Shorthand = ({ shorthandSrcURL, ...props }) => (
<div className="shorthand">
    <div style="overflow-x: visible;" data-shorthand-embed={shorthandSrcURL}></div>
</div>
)

export default Shorthand