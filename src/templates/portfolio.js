import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../components/layout';
import styled from 'styled-components';
import Video from "../components/video"
import Shorthand from "../components/shorthand"


const FeaturedImage = styled.img`
  max-width: 300px;
  margin: 16px 0;
`

export default ({pageContext}) => (
  <Layout>
    <h1>
      {pageContext.title}
    </h1>
    <strong>
      Website url:
    </strong>
    <a href={pageContext.acf.portfolio_url} target="_blank" rel="noopener noreferrer">
      {pageContext.acf.portfolio_url}
    </a>
    <div>
      <h4>Test Shorthand</h4>
      
      <Shorthand
        shorthandSrcURL="demos.shorthandstories.com/embedexample/"
      />
      <Helmet>
        <script src="https://embed.shorthand.com/embed_9.js"></script>
      </Helmet>
      
    </div>
    <Video
        videoSrcURL="https://www.youtube.com/embed/dQw4w9WgXcQ"
        videoTitle="Official Music Video on YouTube"
      />
    <div>
      <FeaturedImage src={pageContext.featured_media.source_url} />
    </div>
    <div dangerouslySetInnerHTML={{__html: pageContext.content}} />
    
  </Layout>
);